﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aval_5118
{
    public class Cliente
    {
        
        private string nome;
        private DateTime datanasc;
        private int cc;
        private string morada;
        private int codpost;
        private string localidade;
        private int contacto;
        /// <summary>
        /// Dados base de todos os clientes, 
        /// repetiçao expectavel uma vez que nao se comunica com base de dados,
        /// cada conta = objecto, mesmo pertencendo ao mesmo cliente.
        /// (utilizado como substituição de BD).
        /// </summary>

        public Cliente()
        {

        }
        Cliente(string nome, int ano, int mes, int dia, int cc,string morada, int codpost, int contacto,string localidade)
        {
            datanasc = new DateTime(ano, mes, dia);
            this.cc = cc;
            this.nome = nome;
            this.morada = morada;
            this.codpost = codpost;
            this.contacto = contacto;
            this.localidade = localidade;
        }
       
        public int CC
        {
            get { return cc; }
            set { cc = value; }
        }

        public int CODPOST
        {
            get { return codpost; }
            set { codpost = value; }
        }

        public int CONTACTO
        {
            get { return contacto; }
            set { contacto = value; }
        }

        public DateTime DATANAS
        {
            get { return datanasc; }
            set { datanasc = value; }
        }

        public string LOCALIDADE
        {
            get { return localidade; }
            set { localidade = value; }
        }

        public string MORADA
        {
            get { return morada; }
            set { morada = value; }
        }

        public string NOME
        {
            get { return nome; }
            set { nome = value; }
        }
    }
}
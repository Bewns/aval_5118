﻿namespace aval_5118
{
    partial class Operacoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.box_levantamento = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.Label_levantamento = new System.Windows.Forms.Label();
            this.bt_deposito = new System.Windows.Forms.Button();
            this.box_deposito = new System.Windows.Forms.TextBox();
            this.label_deposito = new System.Windows.Forms.Label();
            this.bt_transf = new System.Windows.Forms.Button();
            this.box_nib = new System.Windows.Forms.TextBox();
            this.box_val_trans = new System.Windows.Forms.TextBox();
            this.label_NIB = new System.Windows.Forms.Label();
            this.label_trans = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 74);
            this.button1.TabIndex = 0;
            this.button1.Text = "Consultar Saldo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // box_levantamento
            // 
            this.box_levantamento.Location = new System.Drawing.Point(206, 119);
            this.box_levantamento.Name = "box_levantamento";
            this.box_levantamento.Size = new System.Drawing.Size(100, 20);
            this.box_levantamento.TabIndex = 1;
            this.box_levantamento.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 92);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(179, 73);
            this.button2.TabIndex = 2;
            this.button2.Text = "Levantamento";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Label_levantamento
            // 
            this.Label_levantamento.AutoSize = true;
            this.Label_levantamento.Location = new System.Drawing.Point(312, 122);
            this.Label_levantamento.Name = "Label_levantamento";
            this.Label_levantamento.Size = new System.Drawing.Size(85, 13);
            this.Label_levantamento.TabIndex = 3;
            this.Label_levantamento.Text = "Valor a Levantar";
            // 
            // bt_deposito
            // 
            this.bt_deposito.Location = new System.Drawing.Point(12, 171);
            this.bt_deposito.Name = "bt_deposito";
            this.bt_deposito.Size = new System.Drawing.Size(179, 76);
            this.bt_deposito.TabIndex = 4;
            this.bt_deposito.Text = "Deposito";
            this.bt_deposito.UseVisualStyleBackColor = true;
            this.bt_deposito.Click += new System.EventHandler(this.bt_deposito_Click);
            // 
            // box_deposito
            // 
            this.box_deposito.Location = new System.Drawing.Point(206, 200);
            this.box_deposito.Name = "box_deposito";
            this.box_deposito.Size = new System.Drawing.Size(100, 20);
            this.box_deposito.TabIndex = 5;
            this.box_deposito.TextChanged += new System.EventHandler(this.box_deposito_TextChanged);
            // 
            // label_deposito
            // 
            this.label_deposito.AutoSize = true;
            this.label_deposito.Location = new System.Drawing.Point(312, 203);
            this.label_deposito.Name = "label_deposito";
            this.label_deposito.Size = new System.Drawing.Size(88, 13);
            this.label_deposito.TabIndex = 6;
            this.label_deposito.Text = "Valor a Depositar";
            // 
            // bt_transf
            // 
            this.bt_transf.Location = new System.Drawing.Point(12, 253);
            this.bt_transf.Name = "bt_transf";
            this.bt_transf.Size = new System.Drawing.Size(179, 72);
            this.bt_transf.TabIndex = 7;
            this.bt_transf.Text = "Realizar Transferencia";
            this.bt_transf.UseVisualStyleBackColor = true;
            this.bt_transf.Click += new System.EventHandler(this.bt_transf_Click);
            // 
            // box_nib
            // 
            this.box_nib.Location = new System.Drawing.Point(206, 269);
            this.box_nib.Name = "box_nib";
            this.box_nib.Size = new System.Drawing.Size(100, 20);
            this.box_nib.TabIndex = 8;
            this.box_nib.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // box_val_trans
            // 
            this.box_val_trans.Location = new System.Drawing.Point(206, 295);
            this.box_val_trans.Name = "box_val_trans";
            this.box_val_trans.Size = new System.Drawing.Size(100, 20);
            this.box_val_trans.TabIndex = 9;
            this.box_val_trans.TextChanged += new System.EventHandler(this.box_val_trans_TextChanged);
            // 
            // label_NIB
            // 
            this.label_NIB.AutoSize = true;
            this.label_NIB.Location = new System.Drawing.Point(312, 272);
            this.label_NIB.Name = "label_NIB";
            this.label_NIB.Size = new System.Drawing.Size(79, 13);
            this.label_NIB.TabIndex = 10;
            this.label_NIB.Text = "NIB Conta alvo";
            // 
            // label_trans
            // 
            this.label_trans.AutoSize = true;
            this.label_trans.Location = new System.Drawing.Point(312, 298);
            this.label_trans.Name = "label_trans";
            this.label_trans.Size = new System.Drawing.Size(87, 13);
            this.label_trans.TabIndex = 11;
            this.label_trans.Text = "Valor a Transferir";
            // 
            // Operacoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 523);
            this.Controls.Add(this.label_trans);
            this.Controls.Add(this.label_NIB);
            this.Controls.Add(this.box_val_trans);
            this.Controls.Add(this.box_nib);
            this.Controls.Add(this.bt_transf);
            this.Controls.Add(this.label_deposito);
            this.Controls.Add(this.box_deposito);
            this.Controls.Add(this.bt_deposito);
            this.Controls.Add(this.Label_levantamento);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.box_levantamento);
            this.Controls.Add(this.button1);
            this.Name = "Operacoes";
            this.Text = "Operacoes";
            this.Load += new System.EventHandler(this.Operacoes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox box_levantamento;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label Label_levantamento;
        private System.Windows.Forms.Button bt_deposito;
        private System.Windows.Forms.TextBox box_deposito;
        private System.Windows.Forms.Label label_deposito;
        private System.Windows.Forms.Button bt_transf;
        private System.Windows.Forms.TextBox box_nib;
        private System.Windows.Forms.TextBox box_val_trans;
        private System.Windows.Forms.Label label_NIB;
        private System.Windows.Forms.Label label_trans;
    }
}
﻿namespace aval_5118
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_goto_makeacc = new System.Windows.Forms.Button();
            this.bt_goto_ops = new System.Windows.Forms.Button();
            this.box_nib = new System.Windows.Forms.TextBox();
            this.label_consulta = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bt_goto_makeacc
            // 
            this.bt_goto_makeacc.Location = new System.Drawing.Point(562, 279);
            this.bt_goto_makeacc.Name = "bt_goto_makeacc";
            this.bt_goto_makeacc.Size = new System.Drawing.Size(169, 102);
            this.bt_goto_makeacc.TabIndex = 0;
            this.bt_goto_makeacc.Text = "Criar Conta";
            this.bt_goto_makeacc.UseVisualStyleBackColor = true;
            this.bt_goto_makeacc.Click += new System.EventHandler(this.button1_Click);
            // 
            // bt_goto_ops
            // 
            this.bt_goto_ops.Location = new System.Drawing.Point(53, 278);
            this.bt_goto_ops.Name = "bt_goto_ops";
            this.bt_goto_ops.Size = new System.Drawing.Size(168, 105);
            this.bt_goto_ops.TabIndex = 1;
            this.bt_goto_ops.Text = "Consultas/Operações";
            this.bt_goto_ops.UseVisualStyleBackColor = true;
            this.bt_goto_ops.Click += new System.EventHandler(this.bt_goto_ops_Click);
            // 
            // box_nib
            // 
            this.box_nib.Location = new System.Drawing.Point(53, 156);
            this.box_nib.Name = "box_nib";
            this.box_nib.Size = new System.Drawing.Size(168, 20);
            this.box_nib.TabIndex = 2;
            this.box_nib.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label_consulta
            // 
            this.label_consulta.AutoSize = true;
            this.label_consulta.Location = new System.Drawing.Point(50, 179);
            this.label_consulta.Name = "label_consulta";
            this.label_consulta.Size = new System.Drawing.Size(92, 13);
            this.label_consulta.TabIndex = 3;
            this.label_consulta.Text = "NIB para consulta";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label_consulta);
            this.Controls.Add(this.box_nib);
            this.Controls.Add(this.bt_goto_ops);
            this.Controls.Add(this.bt_goto_makeacc);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_goto_makeacc;
        private System.Windows.Forms.Button bt_goto_ops;
        private System.Windows.Forms.TextBox box_nib;
        private System.Windows.Forms.Label label_consulta;
    }
}


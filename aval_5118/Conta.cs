﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace aval_5118
{
    public class Conta : Cliente
    {
        private int nib;
        private double saldo;
        private string titular;
        private bool prazo;
        /// <summary>
        /// Builder de conta a ordem
        /// </summary>
        /// <param name="nomecli"></param>
        /// <param name="nometit"></param>
        /// <param name="val"></param>
        /// <param name="ano"></param>
        /// <param name="mes"></param>
        /// <param name="dia"></param>
        /// <param name="cc"></param>
        /// <param name="morada"></param>
        /// <param name="codpost"></param>
        /// <param name="contacto"></param>
        /// <param name="localidade"></param>
        public Conta()
        {

        }
        public void Transferencia(int nib, double val)
        {
            if (prazo)
            {
                bool found = false;
                int i = 0;
                foreach (var conta in AcessStuff.ContasOrdem)
                {
                    i++;
                    if (conta.Nib == nib)
                    {
                        i--;
                        saldo -= (val + 0.5d);
                        AcessStuff.ContasOrdem[i].Saldo += val;
                        MessageBox.Show($"Transferencia bem sucedida, saldo actual: {this.saldo}");
                        found = true;
                    }
                }
                if (!found)
                {
                    MessageBox.Show("A Conta Alvo n foi encontrada");
                }
                
            }
            else if (!prazo)
            {

            }
        }
        public void Levantamento(double valor)
        {
            if (prazo)
            {
                if ((valor+0.5d) > Saldo)
                {
                    MessageBox.Show("Operação Impossivel, Nao tem saldo disponivel");
                }
                else
                {
                    saldo = saldo - 0.5d - valor;
                    MessageBox.Show($"Retire o seu Dinheiro: {valor}\nO Seu saldo actual = {this.saldo}");
                }
            }
            else
            {
                if (valor > Saldo)
                {
                    MessageBox.Show("Operação Impossivel, Nao tem saldo disponivel");
                }
                else
                {
                    saldo = saldo - valor;
                    MessageBox.Show($"Retire o seu Dinheiro: {valor}\nO Seu saldo actual = {this.saldo}");
                }
            }
        }
        public void Deposito(double valor)
        {
            saldo = saldo + valor;
            MostrarSaldo();
        }
        public void MostrarSaldo()
        {
            MessageBox.Show($" O seu Saldo é de : {this.Saldo}€");
        }
        
        public Conta(string nomecli,string nometit, uint val, DateTime datein, int cc, string morada, int codpost, int contacto, string localidade, bool prazo)
        {
            this.nib = new Random().Next(1000, 9000);
            foreach (var conta in AcessStuff.ContasOrdem)
            {
                if (conta.Nib == this.nib)
                {
                    this.nib = new Random().Next(1000, 9000);
                }
            }
            this.titular = nometit;
            this.NOME = nomecli;
            this.saldo = Convert.ToDouble(val);
            this.CC = cc;
            this.DATANAS = datein;
            this.CONTACTO = contacto;
            this.CODPOST = codpost;
            this.MORADA = morada;
            this.LOCALIDADE = localidade;
            this.Prazo = prazo;
        }

        public double Saldo
        {
            get { return saldo; }
            set { saldo = value; }
        }

        public string Titular
        {
            get { return titular; }
            set { titular = value; }
        }

        public int Nib
        {
            get { return nib; }
        }
        public bool Prazo
        {
            get { return prazo; }
            set { prazo = value; }
        }

        public void Showdados()
        {
            Console.WriteLine("Dados de Conta : \nTitular: {0}\nSaldo: {1}\nNIB: {2}",Titular,Saldo,Nib);
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aval_5118
{
    public partial class acc_create_form : Form
    {
        string nomecli, morada, localidade;
        DateTime datein = new DateTime();
        int contacto, cc, codpost;
        uint val;
        bool prazo = false;
        public acc_create_form()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void acc_create_form_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            nomecli = box_nome.Text;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            datein = data_nasc.Value;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            int.TryParse(box_contacto.Text, out contacto);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label_username_Click(object sender, EventArgs e)
        {

        }

        private void box_codpost_TextChanged(object sender, EventArgs e)
        {
            int.TryParse(box_cc.Text, out codpost);
        }

        private void chkprazo_CheckedChanged(object sender, EventArgs e)
        {
            prazo = true;
        }

        private void box_deposito_TextChanged(object sender, EventArgs e)
        {
            uint.TryParse(box_deposito.Text, out val);
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            int.TryParse(box_cc.Text, out cc);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            localidade = box_localidade.Text;
        }

        private void bt_criarconta_Click(object sender, EventArgs e)
        {
            if (nomecli == null || morada == null || val == 0 || datein == null || cc == 0 || codpost == 0 || localidade == null || contacto == 0)
            {
                MessageBox.Show("Preencha todos os Dados por favor");
            }
            else
            {
                int i;
                AcessStuff.ContasOrdem.Add(new Conta(nomecli, nomecli, val, datein, cc, morada, codpost, contacto, localidade, prazo));
                i = AcessStuff.ContasOrdem.Count - 1;
                this.Close();
                MessageBox.Show($"O seu nib é : {AcessStuff.ContasOrdem[i].Nib}");
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            morada = box_morada.Text;
        }
    }
}

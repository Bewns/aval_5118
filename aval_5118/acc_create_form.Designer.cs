﻿namespace aval_5118
{
    partial class acc_create_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.box_nome = new System.Windows.Forms.TextBox();
            this.data_nasc = new System.Windows.Forms.DateTimePicker();
            this.box_morada = new System.Windows.Forms.TextBox();
            this.Label_nome_box = new System.Windows.Forms.Label();
            this.label_data_nasc = new System.Windows.Forms.Label();
            this.label_morada = new System.Windows.Forms.Label();
            this.box_localidade = new System.Windows.Forms.TextBox();
            this.label_localidade = new System.Windows.Forms.Label();
            this.box_contacto = new System.Windows.Forms.TextBox();
            this.label_contacto = new System.Windows.Forms.Label();
            this.box_deposito = new System.Windows.Forms.TextBox();
            this.label_deposito = new System.Windows.Forms.Label();
            this.bt_criarconta = new System.Windows.Forms.Button();
            this.box_cc = new System.Windows.Forms.TextBox();
            this.label_cc = new System.Windows.Forms.Label();
            this.box_codpost = new System.Windows.Forms.TextBox();
            this.label_cod_post = new System.Windows.Forms.Label();
            this.chkprazo = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // box_nome
            // 
            this.box_nome.Location = new System.Drawing.Point(12, 57);
            this.box_nome.Name = "box_nome";
            this.box_nome.Size = new System.Drawing.Size(260, 20);
            this.box_nome.TabIndex = 0;
            this.box_nome.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // data_nasc
            // 
            this.data_nasc.Location = new System.Drawing.Point(72, 83);
            this.data_nasc.Name = "data_nasc";
            this.data_nasc.Size = new System.Drawing.Size(200, 20);
            this.data_nasc.TabIndex = 2;
            this.data_nasc.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // box_morada
            // 
            this.box_morada.Location = new System.Drawing.Point(12, 109);
            this.box_morada.Name = "box_morada";
            this.box_morada.Size = new System.Drawing.Size(260, 20);
            this.box_morada.TabIndex = 3;
            this.box_morada.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // Label_nome_box
            // 
            this.Label_nome_box.AutoSize = true;
            this.Label_nome_box.Location = new System.Drawing.Point(281, 63);
            this.Label_nome_box.Name = "Label_nome_box";
            this.Label_nome_box.Size = new System.Drawing.Size(82, 13);
            this.Label_nome_box.TabIndex = 4;
            this.Label_nome_box.Text = "Nome Completo";
            // 
            // label_data_nasc
            // 
            this.label_data_nasc.AutoSize = true;
            this.label_data_nasc.Location = new System.Drawing.Point(281, 90);
            this.label_data_nasc.Name = "label_data_nasc";
            this.label_data_nasc.Size = new System.Drawing.Size(102, 13);
            this.label_data_nasc.TabIndex = 5;
            this.label_data_nasc.Text = "Data de nascimento";
            // 
            // label_morada
            // 
            this.label_morada.AutoSize = true;
            this.label_morada.Location = new System.Drawing.Point(281, 116);
            this.label_morada.Name = "label_morada";
            this.label_morada.Size = new System.Drawing.Size(43, 13);
            this.label_morada.TabIndex = 6;
            this.label_morada.Text = "Morada";
            // 
            // box_localidade
            // 
            this.box_localidade.Location = new System.Drawing.Point(12, 135);
            this.box_localidade.Name = "box_localidade";
            this.box_localidade.Size = new System.Drawing.Size(260, 20);
            this.box_localidade.TabIndex = 7;
            this.box_localidade.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label_localidade
            // 
            this.label_localidade.AutoSize = true;
            this.label_localidade.Location = new System.Drawing.Point(281, 142);
            this.label_localidade.Name = "label_localidade";
            this.label_localidade.Size = new System.Drawing.Size(59, 13);
            this.label_localidade.TabIndex = 8;
            this.label_localidade.Text = "Localidade";
            // 
            // box_contacto
            // 
            this.box_contacto.Location = new System.Drawing.Point(12, 161);
            this.box_contacto.Name = "box_contacto";
            this.box_contacto.Size = new System.Drawing.Size(260, 20);
            this.box_contacto.TabIndex = 9;
            this.box_contacto.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // label_contacto
            // 
            this.label_contacto.AutoSize = true;
            this.label_contacto.Location = new System.Drawing.Point(281, 168);
            this.label_contacto.Name = "label_contacto";
            this.label_contacto.Size = new System.Drawing.Size(50, 13);
            this.label_contacto.TabIndex = 10;
            this.label_contacto.Text = "Contacto";
            // 
            // box_deposito
            // 
            this.box_deposito.Location = new System.Drawing.Point(12, 187);
            this.box_deposito.Name = "box_deposito";
            this.box_deposito.Size = new System.Drawing.Size(100, 20);
            this.box_deposito.TabIndex = 13;
            this.box_deposito.TextChanged += new System.EventHandler(this.box_deposito_TextChanged);
            // 
            // label_deposito
            // 
            this.label_deposito.AutoSize = true;
            this.label_deposito.Location = new System.Drawing.Point(119, 194);
            this.label_deposito.Name = "label_deposito";
            this.label_deposito.Size = new System.Drawing.Size(122, 13);
            this.label_deposito.TabIndex = 14;
            this.label_deposito.Text = "Deposito inicial. VAL.Int.";
            this.label_deposito.Click += new System.EventHandler(this.label_username_Click);
            // 
            // bt_criarconta
            // 
            this.bt_criarconta.Location = new System.Drawing.Point(122, 408);
            this.bt_criarconta.Name = "bt_criarconta";
            this.bt_criarconta.Size = new System.Drawing.Size(155, 50);
            this.bt_criarconta.TabIndex = 15;
            this.bt_criarconta.Text = "Criar Conta";
            this.bt_criarconta.UseVisualStyleBackColor = true;
            this.bt_criarconta.Click += new System.EventHandler(this.bt_criarconta_Click);
            // 
            // box_cc
            // 
            this.box_cc.Location = new System.Drawing.Point(12, 213);
            this.box_cc.Name = "box_cc";
            this.box_cc.Size = new System.Drawing.Size(100, 20);
            this.box_cc.TabIndex = 16;
            this.box_cc.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // label_cc
            // 
            this.label_cc.AutoSize = true;
            this.label_cc.Location = new System.Drawing.Point(119, 220);
            this.label_cc.Name = "label_cc";
            this.label_cc.Size = new System.Drawing.Size(95, 13);
            this.label_cc.TabIndex = 17;
            this.label_cc.Text = "Cartão de Cidadão";
            // 
            // box_codpost
            // 
            this.box_codpost.Location = new System.Drawing.Point(12, 239);
            this.box_codpost.Name = "box_codpost";
            this.box_codpost.Size = new System.Drawing.Size(100, 20);
            this.box_codpost.TabIndex = 18;
            this.box_codpost.TextChanged += new System.EventHandler(this.box_codpost_TextChanged);
            // 
            // label_cod_post
            // 
            this.label_cod_post.AutoSize = true;
            this.label_cod_post.Location = new System.Drawing.Point(119, 246);
            this.label_cod_post.Name = "label_cod_post";
            this.label_cod_post.Size = new System.Drawing.Size(61, 13);
            this.label_cod_post.TabIndex = 19;
            this.label_cod_post.Text = "Cod. Postal";
            // 
            // chkprazo
            // 
            this.chkprazo.AutoSize = true;
            this.chkprazo.Location = new System.Drawing.Point(12, 265);
            this.chkprazo.Name = "chkprazo";
            this.chkprazo.Size = new System.Drawing.Size(93, 17);
            this.chkprazo.TabIndex = 20;
            this.chkprazo.Text = "Conta a Prazo";
            this.chkprazo.UseVisualStyleBackColor = true;
            this.chkprazo.CheckedChanged += new System.EventHandler(this.chkprazo_CheckedChanged);
            // 
            // acc_create_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 470);
            this.Controls.Add(this.chkprazo);
            this.Controls.Add(this.label_cod_post);
            this.Controls.Add(this.box_codpost);
            this.Controls.Add(this.label_cc);
            this.Controls.Add(this.box_cc);
            this.Controls.Add(this.bt_criarconta);
            this.Controls.Add(this.label_deposito);
            this.Controls.Add(this.box_deposito);
            this.Controls.Add(this.label_contacto);
            this.Controls.Add(this.box_contacto);
            this.Controls.Add(this.label_localidade);
            this.Controls.Add(this.box_localidade);
            this.Controls.Add(this.label_morada);
            this.Controls.Add(this.label_data_nasc);
            this.Controls.Add(this.Label_nome_box);
            this.Controls.Add(this.box_morada);
            this.Controls.Add(this.data_nasc);
            this.Controls.Add(this.box_nome);
            this.Name = "acc_create_form";
            this.Text = "Criação de Conta";
            this.Load += new System.EventHandler(this.acc_create_form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox box_nome;
        private System.Windows.Forms.DateTimePicker data_nasc;
        private System.Windows.Forms.TextBox box_morada;
        private System.Windows.Forms.Label Label_nome_box;
        private System.Windows.Forms.Label label_data_nasc;
        private System.Windows.Forms.Label label_morada;
        private System.Windows.Forms.TextBox box_localidade;
        private System.Windows.Forms.Label label_localidade;
        private System.Windows.Forms.TextBox box_contacto;
        private System.Windows.Forms.Label label_contacto;
        private System.Windows.Forms.TextBox box_deposito;
        private System.Windows.Forms.Label label_deposito;
        private System.Windows.Forms.Button bt_criarconta;
        private System.Windows.Forms.TextBox box_cc;
        private System.Windows.Forms.Label label_cc;
        private System.Windows.Forms.TextBox box_codpost;
        private System.Windows.Forms.Label label_cod_post;
        private System.Windows.Forms.CheckBox chkprazo;
    }
}
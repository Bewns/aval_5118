﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aval_5118
{
    public partial class Operacoes : Form
    {
        int i = 0, levantamento, u = 0;
        double deposito, transf;
        bool creator;
        
        public Operacoes(int nib)
        {
            foreach (var conta in AcessStuff.ContasOrdem)
            {
                i++;
                if (conta.Nib == nib)
                {
                    creator = true;
                    i--;
                    InitializeComponent();
                    break;
                }else
                {
                    Load += (s,e) => Close();
                }
            }
        }

        private void Operacoes_Load1(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void MyForm(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AcessStuff.ContasOrdem[i].MostrarSaldo();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int.TryParse(box_levantamento.Text, out levantamento);
        }

        private void box_deposito_TextChanged(object sender, EventArgs e)
        {
            double.TryParse(box_deposito.Text, out deposito);
        }

        private void Operacoes_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            int.TryParse(box_nib.Text, out u);
        }

        private void box_val_trans_TextChanged(object sender, EventArgs e)
        {
            double.TryParse(box_val_trans.Text, out transf);
        }

        private void bt_transf_Click(object sender, EventArgs e)
        {
            AcessStuff.ContasOrdem[i].Transferencia(u, transf);
        }

        private void bt_deposito_Click(object sender, EventArgs e)
        {
            if (deposito <= 0)
            {
                MessageBox.Show("Valor inválido");
            }
            else
            {
                AcessStuff.ContasOrdem[i].Deposito(deposito);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (levantamento <= 0)
            {
                MessageBox.Show("Valor inválido");
            }
            else
            {
                AcessStuff.ContasOrdem[i].Levantamento(levantamento);
            }
        }
    }
}
